﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Windows.Forms;
using System.IO;

namespace KIP_PVS_DB.FINAL
{
    class DB
    {
        private static DataSet db;
        public static String fileNameDB = "data_base.xml";
        public static bool stored_db = false;

        public static void initDataBase()
        {
            if (File.Exists(fileNameDB))
            {
                db = new DataSet();
                readDBFromFile();
            }
            else
            {
                create();
            }
        }

        public static void create()
        {
            db = new DataSet("KIP PVS");

            DataTable t = db.Tables.Add("workTable");
            t.Columns.Add("parameter", Type.GetType("System.String"));
            t.Columns.Add("paramType", Type.GetType("System.String"));
            t.Columns.Add("agregat", Type.GetType("System.String"));
            t.Columns.Add("device_sensor", Type.GetType("System.String"));
            t.Columns.Add("device_viewer", Type.GetType("System.String"));
            t.Columns.Add("device_sensor_helper", Type.GetType("System.String"));
            t.Columns.Add("device_viewer_helper", Type.GetType("System.String"));
            t.Columns.Add("device_sensor_type_helper", Type.GetType("System.String"));
            t.Columns.Add("device_viewer_type_helper", Type.GetType("System.String"));
            t.Columns["device_viewer"].Expression = "device_viewer_type_helper + ' / № ' + device_viewer_helper";
            t.Columns["device_sensor"].Expression = "device_sensor_type_helper + ' / № ' + device_sensor_helper";
            t.Columns.Add("dP.min", Type.GetType("System.String"));
            t.Columns.Add("dP.max", Type.GetType("System.String"));
            t.Columns.Add("dP.unit", Type.GetType("System.String"));
            t.Columns.Add("Q.min", Type.GetType("System.String"));
            t.Columns.Add("Q.max", Type.GetType("System.String"));
            t.Columns.Add("Q.unit", Type.GetType("System.String"));
            t.Columns.Add("ID", Type.GetType("System.String"));// Для удобного поиска

            t = db.Tables.Add("deviceTypes");
            t.Columns.Add("typeName", Type.GetType("System.String"));
            t.Columns.Add("typeDevice", Type.GetType("System.String"));// показывающий или датчик

            t = db.Tables.Add("devices");
            t.Columns.Add("typeName", Type.GetType("System.String"));
            t.Columns.Add("number", Type.GetType("System.String"));
            t.Columns.Add("last_check", Type.GetType("System.DateTime"));
            t.Columns.Add("typeDevice", Type.GetType("System.String"));

            t = db.Tables.Add("measuringTypes");
            t.Columns.Add("measuringName", Type.GetType("System.String"));
            t.Columns.Add("measuringGroup", Type.GetType("System.String"));// группа измерения (перепадный, давленческий/разряженческий)
            initMeasuringGroupTable( t );

            stored_db = false;
        }

        private static void initMeasuringGroupTable( DataTable t )
        {
            DataRow r;
            r = t.NewRow(); r["measuringName"] = "Расход"; r["measuringGroup"] = "dp"; t.Rows.Add(r);
            r = t.NewRow(); r["measuringName"] = "Сопротивление"; r["measuringGroup"] = "dp"; t.Rows.Add(r);
            r = t.NewRow(); r["measuringName"] = "Уровень (перепад)"; r["measuringGroup"] = "dp"; t.Rows.Add(r);

            r = t.NewRow(); r["measuringName"] = "Уровень"; r["measuringGroup"] = "p"; t.Rows.Add(r);
            r = t.NewRow(); r["measuringName"] = "Давление"; r["measuringGroup"] = "p"; t.Rows.Add(r);
            r = t.NewRow(); r["measuringName"] = "Разрежение"; r["measuringGroup"] = "p"; t.Rows.Add(r);
            
            r = t.NewRow(); r["measuringName"] = "Температура"; r["measuringGroup"] = "t"; t.Rows.Add(r);
        }

        public static void setupDataGridView( DataGridView dgv_ )
        {
            DataTable t = db.Tables["workTable"];
            DataView dv = new DataView(t);
            dgv_.DataSource = dv;
            dgv_.Columns["agregat"].Visible = false;
            dgv_.Columns["paramType"].Visible = false;
            dgv_.Columns["device_sensor_helper"].Visible = false;
            dgv_.Columns["device_viewer_helper"].Visible = false;
            dgv_.Columns["device_sensor_type_helper"].Visible = false;
            dgv_.Columns["device_viewer_type_helper"].Visible = false;
            dgv_.Columns["ID"].Visible = false;

            dgv_.Columns["dP.min"].Visible = false;
            dgv_.Columns["dP.max"].Visible = false;
            dgv_.Columns["Q.min"].Visible = false;
            dgv_.Columns["Q.max"].Visible = false;
            dgv_.Columns["Q.unit"].Visible = false;
            dgv_.Columns["dP.unit"].Visible = false;

            dgv_.Columns["parameter"].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
            dgv_.Columns["device_sensor"].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
            dgv_.Columns["device_viewer"].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;

            dgv_.Columns["parameter"].HeaderText = "Наименование параметра";
            dgv_.Columns["device_sensor"].HeaderText = "Датчик";
            dgv_.Columns["device_viewer"].HeaderText = "Показывающий прибор";
        }

        public static void setupComboBoxMeasuringType( ComboBox cb )
        {
            DataTable t = db.Tables["measuringTypes"];
            cb.Items.Clear();
            cb.DataSource = t;
            cb.DisplayMember = "measuringName";
        }

        public static void addMeasuringPoint(String paramName, String paramType, String agreg, String dpmin,
                                             String dpmax, String dpunit,
                                             String qmin, String qmax, String qunit)
        {
            DataTable t = DB.db.Tables["workTable"];

            DataRow r = t.NewRow();
            r["parameter"] = paramName;
            r["paramType"] = paramType;
            r["agregat"] = agreg;
            r["dP.min"] = dpmin;
            r["dP.max"] = dpmax;
            r["dP.unit"] = dpunit;
            r["Q.min"] = qmin;
            r["Q.max"] = qmax;
            r["Q.unit"] = qunit;
            r["device_sensor_helper"] = "0";
            r["device_viewer_helper"] = "0";
            r["device_sensor_type_helper"] = "0";
            r["device_viewer_type_helper"] = "0";
            r["ID"] = getID.doID();

            t.Rows.Add(r);
        }

        public static bool addDeviceType(String typeName, String deviceType)
        {
            DataTable t = db.Tables["deviceTypes"];
            DataRow[] rows_ = t.Select("typeName LIKE {0}", typeName);
            if (rows_.Length > 0)
                return false;

            DataRow r = t.NewRow();
            r["typeName"] = typeName;
            r["typeDevice"] = deviceType;
            t.Rows.Add(r);

            return true;
        }

        public static bool addDevice( String typeName, String number )
        {

            return true;
        }

        public static bool setSensor(String sensor_number, String param_id)
        {

            return true;
        }

        public static bool setViewer(String viewer_number, String param_id)
        {

            return true;
        }

        public static void storeDB()
        {
            if (fileNameDB != "")
            {
                db.WriteXml(fileNameDB, XmlWriteMode.WriteSchema);
                stored_db = true;
            }
        }

        public static void readDBFromFile()
        {
            db.ReadXml(fileNameDB, XmlReadMode.ReadSchema);
        }

    }

    public class getID
    {
        private static Random m_rnd = new Random();
        public static string doID()
        {
            Random r = m_rnd;
            string tmp = "";
            string c = "ABCDEFG0123456789";

            for (int i = 0; i < 10; i++)
            {
                int i_t = r.Next(c.Length);
                tmp += c[i_t];
            }

            return tmp;
        }
    }

}
