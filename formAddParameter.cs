﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace KIP_PVS_DB.FINAL
{
    public partial class formAddParameter : Form
    {
        public formAddParameter()
        {
            InitializeComponent();
        }

        private void formAddParameter_Load(object sender, EventArgs e)
        {
            //comboBox1.SelectedIndex = 0;
            //comboBoxUnit.SelectedIndex = 0;
            //setComboBoxByParamType("Расход");
        }

        private void setupComboBoxdPUnit( string paramtype )
        {

        }

        public DialogResult ShowDialog( string agreg, string paramType )
        {
            labelAgregatParamType.Text = String.Format("{0} / {1}", agreg, paramType);
            setupByParamType(paramType);
            comboBox1.SelectedIndex = 0;
            comboBoxUnit.SelectedIndex = 0;
            DialogResult dr = this.ShowDialog();

            if (dr == System.Windows.Forms.DialogResult.OK)
            {
                outBox.paramName = textBoxParamName.Text;
                outBox.paramType = paramType;
                outBox.agregat = agreg;
                outBox.dPmin = textBoxdPmin.Text;
                outBox.dPmax = textBoxdPmax.Text;
                outBox.dPunit = comboBox1.Text;
                outBox.Qmin = textBoxQmin.Text;
                outBox.Qmax = textBoxQmax.Text;
                outBox.Qunit = comboBoxUnit.Text;
            }

            return dr;
        }

        public struct _outBox
        {
            public string paramName;
            public string paramType;
            public string agregat;
            public string dPmin;
            public string dPmax;
            public string dPunit;
            public string Qmin;
            public string Qmax;
            public string Qunit;
        }

        public _outBox outBox/* = new _outBox()*/;

        private void buttonOk_Click(object sender, EventArgs e)
        {
            this.DialogResult = System.Windows.Forms.DialogResult.OK;

            //float str;
            //float.TryParse(textBoxdPmin.Text, out str);
            //MessageBox.Show(str.ToString());

            this.Close();
        }

        private void buttonCancel_Click(object sender, EventArgs e)
        {
            this.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.Close();
        }

        private void textBoxdPmin_KeyDown(object sender, KeyEventArgs e)
        {
            
        }

        private void textBoxdPmin_PreviewKeyDown(object sender, PreviewKeyDownEventArgs e)
        {
            
        }

        private void textBoxdPmin_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == '.') { e.KeyChar = ','; return; }
            //float f;
            //if (!float.TryParse( textBoxdPmin.Text, out f )) e.KeyChar = '\0';
        }

        private void formAddParameter_FormClosing(object sender, FormClosingEventArgs e)
        {
            float f;
            if (!float.TryParse(textBoxdPmin.Text, out f)) 
            {
                if (this.DialogResult == System.Windows.Forms.DialogResult.OK)
                {
                    e.Cancel = true;
                    MessageBox.Show("Не верно введено значение");
                }
            }
        }

        private void setupByParamType( String paramType )
        {
            if (paramType == "dp")
            {
            };
        }

    }
}
