﻿namespace KIP_PVS_DB.FINAL
{
    partial class formAddParameter
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.textBoxParamName = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.labelAgregatParamType = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label2 = new System.Windows.Forms.Label();
            this.textBoxdPmin = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.textBoxdPmax = new System.Windows.Forms.TextBox();
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.textBoxQmin = new System.Windows.Forms.TextBox();
            this.textBoxQmax = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.comboBoxUnit = new System.Windows.Forms.ComboBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.buttonCancel = new System.Windows.Forms.Button();
            this.buttonOk = new System.Windows.Forms.Button();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // textBoxParamName
            // 
            this.textBoxParamName.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.textBoxParamName.Location = new System.Drawing.Point(16, 74);
            this.textBoxParamName.Name = "textBoxParamName";
            this.textBoxParamName.Size = new System.Drawing.Size(481, 26);
            this.textBoxParamName.TabIndex = 0;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label1.Location = new System.Drawing.Point(12, 51);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(231, 20);
            this.label1.TabIndex = 1;
            this.label1.Text = "Наименование параметра";
            // 
            // labelAgregatParamType
            // 
            this.labelAgregatParamType.AutoSize = true;
            this.labelAgregatParamType.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelAgregatParamType.Location = new System.Drawing.Point(6, 9);
            this.labelAgregatParamType.Name = "labelAgregatParamType";
            this.labelAgregatParamType.Size = new System.Drawing.Size(175, 24);
            this.labelAgregatParamType.TabIndex = 2;
            this.labelAgregatParamType.Text = "Котел 2 / Расход";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.labelAgregatParamType);
            this.groupBox1.Location = new System.Drawing.Point(16, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(481, 36);
            this.groupBox1.TabIndex = 3;
            this.groupBox1.TabStop = false;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label2.Location = new System.Drawing.Point(12, 106);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(71, 20);
            this.label2.TabIndex = 1;
            this.label2.Text = "Датчик";
            // 
            // textBoxdPmin
            // 
            this.textBoxdPmin.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.textBoxdPmin.Location = new System.Drawing.Point(113, 106);
            this.textBoxdPmin.Name = "textBoxdPmin";
            this.textBoxdPmin.Size = new System.Drawing.Size(84, 26);
            this.textBoxdPmin.TabIndex = 1;
            this.textBoxdPmin.KeyDown += new System.Windows.Forms.KeyEventHandler(this.textBoxdPmin_KeyDown);
            this.textBoxdPmin.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textBoxdPmin_KeyPress);
            this.textBoxdPmin.PreviewKeyDown += new System.Windows.Forms.PreviewKeyDownEventHandler(this.textBoxdPmin_PreviewKeyDown);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label3.Location = new System.Drawing.Point(208, 114);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(34, 20);
            this.label3.TabIndex = 4;
            this.label3.Text = ". . .";
            // 
            // textBoxdPmax
            // 
            this.textBoxdPmax.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.textBoxdPmax.Location = new System.Drawing.Point(248, 106);
            this.textBoxdPmax.Name = "textBoxdPmax";
            this.textBoxdPmax.Size = new System.Drawing.Size(84, 26);
            this.textBoxdPmax.TabIndex = 2;
            this.textBoxdPmax.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textBoxdPmin_KeyPress);
            // 
            // comboBox1
            // 
            this.comboBox1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.Items.AddRange(new object[] {
            "кгс/см2",
            "кгс/м2",
            "мм. вод. ст.",
            "мм. рт. ст.",
            "Па",
            "кПа",
            "МПа",
            "бар",
            "атм",
            "град. С",
            "т/ч",
            "м3/ч",
            "м3/мин"});
            this.comboBox1.Location = new System.Drawing.Point(376, 106);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(121, 28);
            this.comboBox1.TabIndex = 3;
            // 
            // textBoxQmin
            // 
            this.textBoxQmin.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.textBoxQmin.Location = new System.Drawing.Point(113, 151);
            this.textBoxQmin.Name = "textBoxQmin";
            this.textBoxQmin.Size = new System.Drawing.Size(84, 26);
            this.textBoxQmin.TabIndex = 4;
            this.textBoxQmin.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textBoxdPmin_KeyPress);
            // 
            // textBoxQmax
            // 
            this.textBoxQmax.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.textBoxQmax.Location = new System.Drawing.Point(248, 151);
            this.textBoxQmax.Name = "textBoxQmax";
            this.textBoxQmax.Size = new System.Drawing.Size(84, 26);
            this.textBoxQmax.TabIndex = 5;
            this.textBoxQmax.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textBoxdPmin_KeyPress);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label4.Location = new System.Drawing.Point(12, 151);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(63, 20);
            this.label4.TabIndex = 1;
            this.label4.Text = "Шкала";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label5.Location = new System.Drawing.Point(208, 159);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(34, 20);
            this.label5.TabIndex = 4;
            this.label5.Text = ". . .";
            // 
            // comboBoxUnit
            // 
            this.comboBoxUnit.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxUnit.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.comboBoxUnit.FormattingEnabled = true;
            this.comboBoxUnit.Items.AddRange(new object[] {
            "кгс/см2",
            "кгс/м2",
            "мм. вод. ст.",
            "мм. рт. ст.",
            "Па",
            "кПа",
            "МПа",
            "бар",
            "атм",
            "град. С",
            "т/ч",
            "м3/ч",
            "м3/мин"});
            this.comboBoxUnit.Location = new System.Drawing.Point(376, 148);
            this.comboBoxUnit.Name = "comboBoxUnit";
            this.comboBoxUnit.Size = new System.Drawing.Size(121, 28);
            this.comboBoxUnit.TabIndex = 6;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.buttonCancel);
            this.groupBox2.Controls.Add(this.buttonOk);
            this.groupBox2.Location = new System.Drawing.Point(12, 183);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(485, 55);
            this.groupBox2.TabIndex = 6;
            this.groupBox2.TabStop = false;
            // 
            // buttonCancel
            // 
            this.buttonCancel.Location = new System.Drawing.Point(404, 19);
            this.buttonCancel.Name = "buttonCancel";
            this.buttonCancel.Size = new System.Drawing.Size(75, 23);
            this.buttonCancel.TabIndex = 1;
            this.buttonCancel.Text = "Отменить";
            this.buttonCancel.UseVisualStyleBackColor = true;
            this.buttonCancel.Click += new System.EventHandler(this.buttonCancel_Click);
            // 
            // buttonOk
            // 
            this.buttonOk.Location = new System.Drawing.Point(323, 19);
            this.buttonOk.Name = "buttonOk";
            this.buttonOk.Size = new System.Drawing.Size(75, 23);
            this.buttonOk.TabIndex = 0;
            this.buttonOk.Text = "Принять";
            this.buttonOk.UseVisualStyleBackColor = true;
            this.buttonOk.Click += new System.EventHandler(this.buttonOk_Click);
            // 
            // formAddParameter
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(511, 250);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.comboBoxUnit);
            this.Controls.Add(this.comboBox1);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.textBoxQmax);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.textBoxQmin);
            this.Controls.Add(this.textBoxdPmax);
            this.Controls.Add(this.textBoxdPmin);
            this.Controls.Add(this.textBoxParamName);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Name = "formAddParameter";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Создание нового измерительного параметра";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.formAddParameter_FormClosing);
            this.Load += new System.EventHandler(this.formAddParameter_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox textBoxParamName;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label labelAgregatParamType;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox textBoxdPmin;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox textBoxdPmax;
        private System.Windows.Forms.ComboBox comboBox1;
        private System.Windows.Forms.TextBox textBoxQmin;
        private System.Windows.Forms.TextBox textBoxQmax;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.ComboBox comboBoxUnit;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Button buttonOk;
        private System.Windows.Forms.Button buttonCancel;
    }
}