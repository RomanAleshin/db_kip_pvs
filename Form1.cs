﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace KIP_PVS_DB.FINAL
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private formAddParameter dlgAddParam = new formAddParameter();

        private void button_addMeasuringParameter_Click(object sender, EventArgs e)
        {
            DialogResult dr =
                    dlgAddParam.ShowDialog( comboBox_Agregats.Text, comboBox_MeasuringTypes.Text );

            if (dr == System.Windows.Forms.DialogResult.OK)
            {
                // Кстати как вариант можно передавать в параметре DataRow
                // Это дает возможность редактировать запись
                // Создать строку, если диалог не вернул OK удалить
                DB.addMeasuringPoint
                    (
                        dlgAddParam.outBox.paramName,
                        dlgAddParam.outBox.paramType,
                        dlgAddParam.outBox.agregat,
                        dlgAddParam.outBox.dPmin,
                        dlgAddParam.outBox.dPmax,
                        dlgAddParam.outBox.dPunit,
                        dlgAddParam.outBox.Qmin,
                        dlgAddParam.outBox.Qmax,
                        dlgAddParam.outBox.Qunit
                    );
                DB.storeDB();
            }
        }

        private void setCommentInfo()
        {
            object[] o = { comboBox_Agregats.Text, comboBox_MeasuringTypes.Text };
            label3.Text = String.Format( "{0} / {1}", o );
        }

        private void comboBox_Agregats_SelectedIndexChanged(object sender, EventArgs e)
        {
            setCommentInfo();
            filterByAgregAndParamType(comboBox_Agregats.Text, comboBox_MeasuringTypes.Text);
        }

        private void dataGridView1_RowPrePaint(object sender, DataGridViewRowPrePaintEventArgs e)
        {
            if ((e.RowIndex % 2) == 0)
                dataGridView1.Rows[e.RowIndex].DefaultCellStyle.BackColor = Color.FromArgb(255, 255, 255);
            else if ((e.RowIndex % 2) == 1)
                dataGridView1.Rows[e.RowIndex].DefaultCellStyle.BackColor = Color.FromArgb(240, 240, 240);
        }

        private void dataGridView1_SelectionChanged(object sender, EventArgs e)
        {
            if (dataGridView1.SelectedRows.Count == 0) return;
            object[] str1 = { dataGridView1.SelectedRows[0].Cells["dP.min"].Value,
                              dataGridView1.SelectedRows[0].Cells["dP.max"].Value,
                              dataGridView1.SelectedRows[0].Cells["dP.unit"].Value };
            object[] str2 = { dataGridView1.SelectedRows[0].Cells["Q.min"].Value,
                              dataGridView1.SelectedRows[0].Cells["Q.max"].Value,
                              dataGridView1.SelectedRows[0].Cells["Q.unit"].Value };
            label6.Text = String.Format("{0} ... {1} {2}", str1);
            label7.Text = String.Format("{0} ... {1} {2}", str2);
        }

        private void comboBox_MeasuringTypes_SelectedIndexChanged(object sender, EventArgs e)
        {
            setCommentInfo();
            filterByAgregAndParamType(comboBox_Agregats.Text, comboBox_MeasuringTypes.Text);
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            DB.initDataBase();
            DB.setupDataGridView(dataGridView1);
            DB.setupComboBoxMeasuringType(comboBox_MeasuringTypes);

            comboBox_Agregats.SelectedIndex = 0;
            comboBox_MeasuringTypes.SelectedIndex = 0;
        }

        private void dataGridView1_UserDeletingRow(object sender, DataGridViewRowCancelEventArgs e)
        {
            DialogResult dr =
                MessageBox.Show( "Параметр будет удален, подтвердите", "ВНИМАНИЕ", MessageBoxButtons.OKCancel, MessageBoxIcon.Warning );
            if (dr == System.Windows.Forms.DialogResult.Cancel)
                e.Cancel = true;
        }

        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            DB.storeDB();
        }

        private void filterByAgregAndParamType( string agreg, string param_type )
        {
            string filter = String.Format("paramType LIKE '{0}' AND agregat LIKE '{1}'",
                param_type, agreg);
            DataView dv = this.dataGridView1.DataSource as DataView;
            dv.RowFilter = filter;
        }

    }
}
